# odu-components

## Hello world

### Usage

Use this component to say `hello` to the World!

```yml
include
  - component: gitlab.com/gl-demo-ultimate-odupre/templates/odu-components/hello@<VERSION>
```

Where `<VERSION>` is the latest release tag or `main`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `greetings` | `world`      | The people targetted nby this warm hello |

## Add dependency

Please read the specific [README.md](./templates/add-dependency/README.md).