# Add dependency

Adds this project as a dependency to the file `application/dependency.yaml`

If it does not exist, the file is created.

Then, current path, branch and project ID are added as a new entry in the YAML file, which is then exposed as an artifact.

## Example

The resulting output looks like the following:

```yaml
dependencies:
  - path: gl-demo-ultimate-odupre/templates/odu-components
    branch: 1.3.1
    projectId: "53401675"
```